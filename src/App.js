import './App.css';

import HouseCards from './HouseCards/HouseCards'
import React, { useState, useEffect } from "react";
import axios from 'axios';
import LazyLoad from 'react-lazyload';

const URL = "https://gist.githubusercontent.com/huvber/b51c0279d3f452513a7c1f576a54f4d7/raw/4497a12b181713c6856303a666d240f7d561e4fe/mock-house"

function App() {
  const [data, setData] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        URL,
      );
      setData(result.data);
    };
 
    fetchData();
  }, []);

  return (
    <div className="App">
      <ul>
        {data.map(item => (
          <LazyLoad key={item.id} height={50} offset={100}>
            <li key={item.id}>
              <HouseCards
                id={item.id}
                image={item.image}
                name={item.name}
                bookable={item.bookable}
                bookedDays={item.booked}
              ></HouseCards>
            </li>
          </LazyLoad>
        ))}
      </ul>
    </div>
  );

}

export default App;
