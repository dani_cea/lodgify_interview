import './HouseCardsText.css';

import React from 'react';

const HouseCardText = ({ id, title }) => (
    <div className="titleContainer">
        <div className="textId">Id: {id}</div>
        <div className="textTitle">{title}</div>
    </div>
);
 
export default HouseCardText;