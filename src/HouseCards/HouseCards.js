import './HouseCards.css';

import React from 'react';

import HouseCardImage from './HouseCardsImage'
import HouseCardButton from './HouseCardsButton' 
import HouseCardText from './HouseCardsText' 
import HouseCardPill from './HouseCardsPill' 

export default function HouseCards({
    id,
    image,
    name,
    bookable,
    bookedDays
}) {
    return(
    <div className={"HouseCardContainer " + (!bookedDays && bookable ? "available" : "")}>
        <HouseCardPill bookable={bookable} bookedDays={bookedDays}></HouseCardPill>
        <HouseCardImage image={image}></HouseCardImage>
        <div className="HouseCardContainerInfo">
            <HouseCardText id={id} title={name}></HouseCardText>
            <HouseCardButton bookable={bookable} bookedDays={bookedDays}></HouseCardButton>
        </div>
    </div>
    );
}