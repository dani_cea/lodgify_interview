import './HouseCardsImage.css';

import React, { useState } from 'react';
import placeholder from '../Placeholder.png';
 
export default function HouseCardImage({
  image
}) {
  const [loaded, setLoaded] = useState(false);

  function showImage() {
    setLoaded({loaded: true})
  }

  return (
    <>
      <div className="bookedMessage"></div>
      <img 
        alt="Placeholder"
        className="image" 
        src={placeholder}
        style={{ display: loaded ? "none" : "block"}} 
      />
      <img
        alt="House" 
        className="image" 
        onLoad={showImage} 
        style={{ display: loaded ? "block" : "none"}} 
        src={image}
      />
    </>
  );
}