import './HouseCardsButton.css';

import React from 'react';
import { IoCheckmarkCircleOutline } from 'react-icons/io5';

export default function HouseCardButton({
    bookedDays,
    bookable
}) {
    if (bookedDays > 0) {
        return (
            <button className="button booked"> 
                <div className="bookedMessage">
                    <IoCheckmarkCircleOutline className="tickIcon"/> {`Booked for ${bookedDays} days`}
                </div>
            </button>
        );
    } else {
        return (
            <button className={"button " + (bookable ? "available" : "unavailable")}> 
                {bookable ? "Book" : "Not Bookable"}
            </button>
        );
    }
}