import './HouseCardsPill.css';

import React from 'react';

export default function HouseCardPill({
    bookedDays,
    bookable
}) {
    var isBooked = bookedDays > 0;


    function getPillType() {
        if (isBooked) {
            return "booked"
        } else {
            return bookable ? "available" : "unavailable"
        }
    }

    return (
        <div className={"pill " + getPillType()}> 
            {getPillType()}
        </div>
    );
}