# Lodgify Grid

This project was created using [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites

Node (with npm) to install dependencies

## Installing dependencies

Run the following command:
```
npm install
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.


## Developer comments

 1. Grid implemented using CSS Grid with a media query to adjust the columns for Mobile devices
 2. All the cards are implemented using a Lazy Loader, so only whatever 
    has to be seen in the viewport is rendered. 
 3. A placeholder is used as image until the third party image has finished loading.